--------------------------
Ubercart CCK Checkout Pane
--------------------------

Maintained by Martin B. - martin@webscio.net
Supported by JoyGroup - http://www.joygroup.nl


Introduction
------------
This module allows you to define CCK nodes as checkout panes in Ubercart.
